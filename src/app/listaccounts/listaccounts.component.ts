import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from "@angular/material/paginator";
import {MatSort} from "@angular/material/sort";
import {AccountsList} from "../accounts-list";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {map, tap} from "rxjs/operators";
import {Accounts} from "../accounts";
import {Observable} from "rxjs";


@Component({
  selector: 'app-listaccounts',
  templateUrl: './listaccounts.component.html',
  styleUrls: ['./listaccounts.component.less']
})
export class ListaccountsComponent implements OnInit {
  private apiUrl = 'http://localhost:8080/';
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  dataSource: AccountsList[];

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private http: HttpClient) {

  }

  ngOnInit(): void {
    //this.dataSource = new MatTableDataSource(this.getAccounts());
    this.http.get(this.apiUrl).subscribe((data: any[]) => {
      console.log(data);
      this.dataSource = data;
    });
  }

  getAccountsList(): Observable<Accounts>{
    return this.http.get<Accounts>(this.apiUrl).pipe(
      tap((response: Accounts) => console.log(response.accountsList))
    );
  }

}
