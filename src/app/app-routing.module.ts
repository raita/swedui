import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {ListaccountsComponent} from "./listaccounts/listaccounts.component";
import {AddaccountComponent} from "./addaccount/addaccount.component";


const routes: Routes = [
  {path: 'listaccounts', component: ListaccountsComponent},
  {path: 'addaccount', component: AddaccountComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
