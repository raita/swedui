import { Component, OnInit } from '@angular/core';

import {FormBuilder, FormControl, FormGroup, ReactiveFormsModule} from '@angular/forms';
import {HttpClient, HttpHeaders} from "@angular/common/http";

@Component({
  selector: 'app-addaccount',
  templateUrl: './addaccount.component.html',
  styleUrls: ['./addaccount.component.less']
})
export class AddaccountComponent implements OnInit {
  addAccountForm = new FormGroup({
    accountOwner: new FormControl(),
    currency: new FormControl()
  });

  private apiUrl = 'http://localhost:8080/';
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  constructor(private formBuilder: FormBuilder, private http: HttpClient) {
    this.createForm();
  }

  createForm(){
    this.addAccountForm = this.formBuilder.group({
      accountOwner: '',
      currency: ''
    });
  }

  onSubmit(){
    console.log(this.addAccountForm.getRawValue());
    this.http.post(this.apiUrl+"addaccount", this.addAccountForm.getRawValue(), this.httpOptions)
      .subscribe();
  }

  ngOnInit(): void {
  }

}
