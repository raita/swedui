export class AccountsList {
  accountNumber: string;
  accountOwner: string;
  balance: number;
  currency: string;
  status: number;
}
